import { expect } from 'chai'
import { async } from 'regenerator-runtime'
import { getUserByUsername } from './db'
import { getDatabaseData, resetDatabase, setDatabaseData } from './test-helpers'

describe('getUserByUsername', () => {
    afterEach('reset the database', async () => {
        await resetDatabase()
    })
    it('get the correct user from the database given a username', async () => {
        const fakeData = [
            {
                id: '123',
                username: 'najib',
                email: 'najib@email.com'
            },
            {
                id: '124',
                username: 'raihan',
                email: 'raihan@email.com'
            }
        ]

        await setDatabaseData('users', fakeData)
        const actual = await getUserByUsername('najib')
        const finalDBState = await getDatabaseData('users')

        const expected = {
            id: '123',
            username: 'najib',
            email: 'najib@email.com'
        }

        expect(actual).excludingEvery('_id').to.deep.equal(expected)
        expect(finalDBState).excludingEvery('_id').to.deep.equal(fakeData)
    })

    it('returns null when the user is not found', async () => {
        
    })
})